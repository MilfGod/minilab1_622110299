using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeControl2 : MonoBehaviour
{
    private Vector3 _cubeMovementStep = new Vector3(0.03f, 0.0f, 0);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += _cubeMovementStep;

        if (this.transform.position.x >= 5.0f || this.transform.position.x <= -5.0f)
        {
            _cubeMovementStep.x *= -1;
        }

        if (this.transform.position.y >= 5.0f || this.transform.position.y <= -5.0f)
        {
            _cubeMovementStep.y *= -1;
        }

        if (this.transform.position.z >= 5.0f || this.transform.position.z <= -5.0f)
        {
            _cubeMovementStep.z *= -1;
        }

        if (Input.GetMouseButtonDown(0))
        {
            GameObject cube = GameObject.Find("Cube");
            Vector3 cubePosition = cube.transform.position;
            Vector3 vecToCubeNorm = cubePosition - this.transform.position;
            vecToCubeNorm.Normalize();

            this._cubeMovementStep = vecToCubeNorm / 10.0f;
        }
    }
}